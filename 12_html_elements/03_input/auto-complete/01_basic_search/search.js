$(document).ready(function () {
    var timeout; // Timer for delaying AJAX request

    $('#searchInput').on('input', function () {
        var searchValue = $(this).val();
        clearTimeout(timeout); // Clear previous timer

        // Delay the AJAX request to avoid sending requests for every keystroke
        timeout = setTimeout(function () {
            if (searchValue.length >= 2) { // Perform search when at least 3 characters are entered
                $.ajax({
                    url: 'data.json', // Path to your static JSON file
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        displayResults(response, searchValue);
                    },
                    error: function (xhr, status, error) {
                        console.error('Error:', error);
                    }
                });
            }
        }, 300); // Adjust the delay time as needed
    });

    function displayResults(results, searchValue) {
        var searchResults = $('#searchResults');
        searchResults.empty(); // Clear previous results

        var matchingResults = results.filter(function (result) {
            return result.toLowerCase().includes(searchValue.toLowerCase());
        });

        if (matchingResults.length > 0) {
            $.each(matchingResults, function (index, result) {
                var listItem = $('<li>').text(result);
                searchResults.append(listItem);
            });
        } else {
            searchResults.append('<li>No results found</li>');
        }
    }
});
