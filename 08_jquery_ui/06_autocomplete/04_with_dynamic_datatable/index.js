$(document).ready(function () {
    var data = [
        { name: "John", age: 25 },
        { name: "Jane", age: 30 },
        { name: "Jim", age: 28 },
    ];

    var table = $("#example").DataTable({
        data: data,
        columns: [
            {
                data: "name",
                render: function (data, type, row) {
                    return (
                        '<input type="text" class="autocomplete" value="' + data + '" placeholder="Type here...">'
                    );
                },
            },
            { data: "age" },
        ],
    });

    // Initialize autocomplete for dynamically created input fields
    $("tbody").on("focus", ".autocomplete", function () {
        $(this).autocomplete({
            source: data.map((item) => item.name),
            minLength: 0,
        });
    });
});