$(document).ready(function () {
    $("#example").DataTable();

    $(".autocomplete").autocomplete({
        source: function (request, response) {
            // Simulate fetching data from a remote source
            var data = ["John", "Jane", "Jim", "Jessica"];
            // Filter data based on the user's input
            var filteredData = $.grep(data, function (item) {
                return (
                    item
                        .toLowerCase()
                        .indexOf(request.term.toLowerCase()) !== -1
                );
            });
            response(filteredData);
        },
        minLength: 1, // Minimum characters to trigger autocomplete
        select: function (event, ui) {
            // Handle selection of an item from autocomplete dropdown
            // For example, you may want to populate other fields based on the selection
            console.log("Selected: ", ui.item.value);
        },
        open: function () {
            // Add custom class to autocomplete menu for styling
            $(this)
                .autocomplete("widget")
                .addClass("autocomplete-menu");
        },
        close: function () {
            // Remove custom class when autocomplete menu is closed
            $(this)
                .autocomplete("widget")
                .removeClass("autocomplete-menu");
        },
        search: function () {
            // Show loading indicator while searching
            $(this)
                .closest(".autocomplete-container")
                .find(".autocomplete-loading")
                .show();
        },
        response: function () {
            // Hide loading indicator after data is fetched
            $(this)
                .closest(".autocomplete-container")
                .find(".autocomplete-loading")
                .hide();
        },
    });
});