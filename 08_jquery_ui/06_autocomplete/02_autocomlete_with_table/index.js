$(document).ready(function () {
    var storedJsonData; // Variable to store the retrieved JSON data

    // Retrieve JSON data and store it in the 'jsonData' variable
    $.ajax({
        url: 'data.json',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            storedJsonData = data; // Store the data in the variable
        },
        error: function (xhr, status, error) {
            console.error('Error:', error);
            storedJsonData = []; // Set empty array in case of error
        }
    });

    $('.autocomplete').autocomplete({
        source: function (request, response) {
            var userNameColumn = $(this.element).data('column');//userName
            var matchedRows = storedJsonData.filter(function (item) {
                return item[userNameColumn].toLowerCase().indexOf(request.term.toLowerCase()) !== -1;
            });
            var matchedRowsSingleColumn = matchedRows.map(function (item) {
                return item[userNameColumn];
            });
            response(matchedRowsSingleColumn);
        },
        minLength: 1,
        select: function (event, ui) {
            var selectedInput = $(event.target);
            var closestTd = selectedInput.closest('td');
            var closestTr = closestTd.closest('tr');
            var selectedColumnName = selectedInput.data('column');
            var selectedValue = ui.item.value;
            selectedInput.val(selectedValue);

            // Find the index of the column in the row
            var columnIndex = closestTd.index();

            // Update other cells in the same row based on the selected value
            closestTr.find('td').each(function (index) {
                if (index !== columnIndex) {
                    var previousColumn = $(this).find('input').data('column');
                    var matchingSingleJsonItem = storedJsonData.filter(function (item) {
                        return item[selectedColumnName] === selectedValue;
                    })[0];
                    $(this).find('input').val(matchingSingleJsonItem[previousColumn]);
                }
            });

            // Close the autocomplete dropdown
            $(this).autocomplete('close');
            return false; // Prevent default behavior of jQuery UI Autocomplete
        }
    });
});
