$('#example').dataTable({
    "ajax": {
        "type": 'GET',
        "dataType": 'json',
        "async": false,
        "url": "author_editor.json",
    },
    "columns":
        [
            {
                "data": "Id",
                "defaultContent": ""
            },
            {
                "data": "Author",
                "defaultContent": null,
                "render": (data, type, row) => {
                    return $.map(data, function (d, i) {
                        return `${d.given} ${d.family}`;
                    }).join(',<br />');
                }
            },
            {
                "data": "Editor",
                "defaultContent": null,
                "render": (data, type, row) => {
                    // return $.map(data,  (d, i) => `${d.given} ${d.family}`).join(',<br />');
                    return data.map(x => `${x.given}, </br> ${x.family}`);
                }
            }
        ],
    "columnDefs":
        [
            {
                "targets": [3],
                "render": (data, type, row) => {
                    var joined = row['Author'].concat(row['Editor']);
                    return $.map(joined, function (d, i) {
                        return `${d.given} ${d.family}`;
                    }).join(',<br />');
                }
            }
        ]
});