$(document).ready(function () {
    $('#example').DataTable({
        "processing": true,
        "serverSide": false,
        "ajax": "./employees.json",
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "position" },
            { "data": "office" },
            { "data": "age" },
            { "data": "start_date" },
            {
                "data": "salary",
                "render": function (data, type, row) {
                    return '<input type="text" value="' + data + '">';
                }
            }
        ],
    });
});