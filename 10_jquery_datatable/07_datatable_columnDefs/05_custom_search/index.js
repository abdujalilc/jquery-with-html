$(document).ready(function () {
    $('#example').DataTable({
        "processing": true,
        "serverSide": false,
        "ajax": "./employees.json",
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "position" },
            { "data": "office" },
            { "data": "age" },
            { "data": "start_date" },
            { "data": "salary" }
        ],
        "columnDefs": [
            { "searchable": false, "targets": [0, 1, 2, 3, 4, 5, 6] } // Make all columns not searchable
        ]
    });

    // Custom search input
    $('#customSearch').on('keyup', function () {
        $('#example').DataTable().search(
            $('#customSearch').val()
        ).draw();
    });
});