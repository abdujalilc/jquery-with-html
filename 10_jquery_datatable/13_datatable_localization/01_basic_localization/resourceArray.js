var resourceArray = {
    "uz": {
        "create": "Qo'shish",
        "createRange": "Diapazon yaratish",
        "language": "Til",
        "updateResource": "Resursni yangilash",
        "keyWord": "Kalit So'z",
        "edit": "Tahrirlash",
        "value": "Qiymat",
        "cultureName": "Til nomi"
    },
    "ru": {
        "create": "Создать",
        "createRange": "Создать диапозон",
        "language": "Язык",
        "updateResource": "Обновить ресурс",
        "keyWord": "Ключевое слово",
        "edit": "Редактировать",
        "value": "Значение",
        "cultureName": "Название культур"
    },
    "en": {
        "create": "Create",
        "createRange": "Create Range",
        "language": "Language",
        "updateResource": "Update Resource",
        "keyWord": "Key Word",
        "edit": "Edit",
        "value": "Value",
        "cultureName": "Culture Name"
    }
}