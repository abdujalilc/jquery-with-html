function addDataTable() {
    var culture = $("#cultureName").val();

    var options = {
        "ajax": {
            "type": 'GET',
            "dataType": 'json',
            "async": false,
            "url": "datatable/resource.json",
        },
        "order": [0, "asc"],
        "columns": [
            { "data": "ID", "name": "ID", "autoWidth": true },
            { "data": "KeyName", "name": "KeyName", "autoWidth": true },
            { "data": "Value", "name": "Value", "autoWidth": true },
            { "data": "LanguageName", "name": "LanguageName", "autoWidth": true },
            {
                "data": null,
                "name": "ID",
                "render": function (data, type, row) {
                    var editText = {
                        "en": "Edit",
                        "ru": "Редактировать",
                        "uz": "Tahrirlash",
                    };
                    var translation = editText[culture] || "Edit";
                    return '<a href="#">' + translation + '</a>';
                },
                "sort": false
            },
        ]
    };

    if (culture !== 'en') {
        options.language = {
            "url": "datatable/" + culture + ".json"
        };
    }

    var table = $("#tblResources").DataTable(options);

    $('#tblResources').find('th').each(function (index, element) {
        var key = $(this).attr('key');
        if (key) {
            $(this).text(resourceArray[culture][key]);
        }
    });
}