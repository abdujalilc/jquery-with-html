$('#dataTable').DataTable({
    ajax: 'data.json',
    columns: [        
        { data: 'id', title: 'Id' },
        { data: 'name', title: 'Name', className: 'editable' },
        { data: 'age', title: 'Age', className: 'editable' },
        { data: 'country', title: 'Country', className: 'editable' }
    ]
});

$('#dataTable').on('click', 'td.editable', function () {
    $(this).attr('contenteditable', 'true').focus();
});

$('#dataTable').on('blur', 'td.editable', function () {
    $(this).removeAttr('contenteditable');
});