$('#saveChanges').on('click', function () {
    if (editedRows.length === 0) {
        alert('No changes to save.');
        return;
    }

    // Send editedRows data or save to file
    console.log('Edited Rows:', editedRows);

    // Example: Export JSON
    const blob = new Blob([JSON.stringify(editedRows, null, 2)], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'edited_data.json';
    a.click();
    URL.revokeObjectURL(url);

    // Clear the edited rows array
    editedRows = [];
});