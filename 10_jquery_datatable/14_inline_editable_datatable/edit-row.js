let editedRows = [];

// Capture changes and store edited rows
$('#dataTable').on('blur keypress', 'td.editable', function (e) {
    if (e.type === 'blur' || e.type === 'focusout' || (e.type === 'keypress' && e.key === 'Enter')) {
        const table = $('#dataTable').DataTable();
        const cell = table.cell(this);
        const rowData = table.row($(this).closest('tr')).data();

        // Mark row as edited if not already tracked
        const editedRowIndex = editedRows.findIndex(row => row.id === rowData.id);
        if (editedRowIndex === -1) {
            editedRows.push(rowData);
        } else {
            editedRows[editedRowIndex] = rowData;
        }

        // Save the new cell value
        cell.data($(this).text()).draw();

        // Remove focus after Enter key
        if (e.type === 'keypress') {
            $(this).blur();
        }
    }
});
