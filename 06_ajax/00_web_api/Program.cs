using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddCors();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

WebApplication? app = builder.Build();

app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
#region getMethods
app.MapGet("/", () =>
{
    return "<h3>Some news of the world</h3><h5>05.01.2023</h5>";
});

app.MapPost("/", (HttpContext httpContext) =>
{
    string? Events = httpContext?.Request?.Form["events"] ?? "";
    string? Dates = httpContext?.Request?.Form["dates"] ?? "";
    return $"<h3>{Events}</h3><h5>{Dates}</h5>";
});

app.MapGet("/json", () => Results.Json(
    new
    {
        @event = "Some news from api",
        @date = "26.01.2013"
    })
);
#endregion getMethods
app.MapPost("/post", (HttpContext httpContext) =>
{
    string? login = httpContext?.Request?.Form["login"] ?? "";
    string? password = httpContext?.Request?.Form["password"] ?? "";
    if (login == "1111" && password == "2222")
        return "Authorization is successful";
    else
        return "Wrong login or password";
});
app.MapPost("/postserial", () => { });

app.MapGet("/ajax", (string name) =>
{
    PersonModel person = new PersonModel
    {
        Name = name,
        DateTime = DateTime.Now.ToString()
    };
    return Results.Json(person);
});

app.UseSwagger();
app.UseSwaggerUI();
app.Run();
