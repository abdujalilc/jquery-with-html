var resources = {
  en: {
    translation: {
      title: "Language Switcher Example",
      hello: "Hello.",
      welcome: "Welcome.",
    },
  },
  ru: {
    translation: {
      title: "Пример переключателя языка",
      hello: "Привет.",
      welcome: "Добро пожаловать.",
    },
  },
  uz: {
    translation: {
      title: "Til o'zgartiruvchi misol",
      hello: "Salom.",
      welcome: "Xush kelibsiz.",
    },
  },
};