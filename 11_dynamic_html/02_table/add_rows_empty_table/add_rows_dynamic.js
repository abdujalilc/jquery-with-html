function initFunc(){
	var b = 1;
	//<a id="add_row">Add Row</a>
	$("#add_row").click(function () {
		//empty table row: <tr id='addr1'></tr>
		$('#addr' + b).html(
			//adding td's
			"<td>" + (b + 1) + "</td>"+
			"<td>"+
			"	<input value=''>"+ 
			"</td>"+
			" <td>"+
				"<input value=''>"+
			"</td>"+
			"<td>" +
				  "<select name='AssestmentMarkQuantityLst["+b+"].CategoryId' class='form-control'>"+
					   "@foreach (var item in ViewBag.ascategories as IEnumerable<SelectListItem>)"+
					"{"+
						"<option value='@item.Value'>@item.Text</option>"+
					"}"+
				"</select>"+ 
			"</td");
		//<table id="tab_logic">	
		$('#tab_logic').append('<tr id="addr' + (b + 1) + '"></tr>');	
		//set new sequence number
		b++;
	});	
	$("#delete_row").click(function () {
		//just empty table row
		if (b > 1) {
			$("#addr" + (b - 1)).html('');
			b--;
		}
	});
}
